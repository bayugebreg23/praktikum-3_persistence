package com.bayudwi_10191016.praktikum3_persistence;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    // Deklarasi Variabel Pendukung
    private TextView Hasil;
    private EditText Masukan;
    private Button Eksekusi;

    // Deklarasi dan Inisialisasi SharedPreferences
    private SharedPreferences preferences;

    // Digunakan untuk Konfigurasi SharedPreferences
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Masukan = findViewById(R.id.input);
        Hasil = findViewById(R.id.output);
        Eksekusi = findViewById(R.id.save);

        // Membuat File Baru beserta modifiernya
        preferences = getSharedPreferences("Belajar_SharedPrreferences", Context.MODE_PRIVATE);

        Eksekusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(getApplicationContext(), "Data Tersimpan", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getData() {
        // Mendapatkan Input dari user
        String getKonten = Masukan.getText().toString();

        // Digunakan untuk pengaturan konfigurasi SharePreferences
        editor = preferences.edit();

        // Memasukkan data pada editor SharedPreferences dengan key (Data_saya)
        editor.putString("data_saya", getKonten);

        // Menjalankan Operasi
        editor.apply();

        // Menampilkan Output
        Hasil.setText("Output Data : " + preferences.getString("data_saya", null));
    }
}